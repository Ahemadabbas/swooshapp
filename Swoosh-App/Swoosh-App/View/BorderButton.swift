//
//  BorderButton.swift
//  Swoosh-App
//
//  Created by AhemadAbbas Vagh on 12/05/19.
//  Copyright © 2019 AhemadAbbas Vagh. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 2.0
        self.layer.borderColor = UIColor.white.cgColor
    }

}
